var doFade = function (dueID, due, inclass) {
  
  // Convert to strings...
  var due = "" + due;
  var isInClass = "" + inclass;
  
  
  if (isValidDueDate(due)) {
    var today     = moment();
    var todayString = today.format("ddd, MMM Do");
    var m = moment();
    
    m = parseDueDate(due);
  
    var diff = m.diff(today, 'days');
    var diffm = m.diff(today, 'minutes');
    
    //console.log(dueID + " inclass: " + isInClass + " diff: " + diff + " diffm: " + diffm);
    // Do Fade?
    if ((diff <= 0) && (diffm <= 0)) {
      //console.log("Setting fade-" + dueID);
      $("#" + "fade-" + dueID).css({
        "opacity" : .6,
      });
    
    }
  }
};


function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}

var dueIn = function (dueID, due, isInClass, prefix, scale, usestars) {
  var due = "" + due;
  var isInClass = "" + isInClass;
  //console.log("isInClass: " + isInClass);
  
  if (isValidDueDate(due)) {
    var today     = moment();
    var todayString = today.format("ddd, MMM Do");
    var m = moment();
    
    m = parseDueDate(due);
  
    var diff = m.diff(today, 'days');
    var diffh = m.diff(today, 'hours');
    var diffm = m.diff(today, 'minutes');
    
    var difference = prefix + " ";
    difference += m.from(moment());
    differenceHTML = "";
    
    //console.log(dueID + "-" + due + " diff: " + diff + " diffm: " + diffm);
    
    labeltype = "";
    labelmsg  = "";
    alerts = ["update", "alert", "newsflash"];
    console.log("IC: " + isInClass + " : " + ((isInClass.length > 0) && (isInClass != "true")));
    
    if (((isInClass.length > 0) && (isInClass != "true"))) {
      labeltype = "label-warning";
      labelmsg  = isInClass;
    } else {
      labeltype = "label-info";
      labelmsg  = "in class";
    }
        
    if (isInClass != false) {
      // console.log("Categories: " + categories);
      differenceHTML += "<span style='font-size: " 
                    + scale + ";'"
                    + "class = 'label " + labeltype + " pull-right'>"
                    + labelmsg 
                    + "</span>";
    } else {
        if (usestars && (diff < 7) && (diff >= 0) && (diffh >= 0) && (diffm > 3)) {
          star = getIcon("star");
          if (usestars == "true") {
            differenceHTML += "<span style='font-size: small; color: {{site.data.course.starcolor}};'>" + star + " TODO " + star + "</span>";
          }
            
          }
        
        differenceHTML += "<span style='font-size: " + scale + ";' class='label " + 
          getRangeColor(m) + 
          " pull-right'>" +
          difference + 
          "</span>";
    } //end if/else in class

    } else { //Invalid due date.
      console.log ("Invalid due date: " + due + "-" + dueID);
    }// end isValidDueDate
  
  
  $("#" + dueID).html(differenceHTML);
};

var prettyDate = function (elem, due, format) {
  m = parseDueDate(due);
  $("#" + elem).html(m.format("ddd, MMM Do"));
};