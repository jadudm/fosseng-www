---
---

Last week, I introduced you to tools that you can use to automatically keep track of your colleagues and community.

## Reading our Colleagues' Writing

* [Feedly](http://feedly.com/)

    This is a web-based RSS reader. You can also consider [RSSOwl](http://www.rssowl.org/), which is a native RSS reader. It is FOSS software, and runs on Mac, Windows, and Linux.

* [IFTTT](https://ifttt.com/)

    A simple way to tie things together. If you want to automatically pull from your colleagues' RSS feeds and (say) generate an email to yourself, or post to a Slack channel, or turn on a coffee maker, then this is the tool for you.

I would like you to be regularly (several times a week) be reading your colleagues' writing. In this regard, I would encourage you to make sure that your blogs are (generally) topical, substantive, and clear.

## Regarding your Blogging

While we are not writing academic prose that might serve as a final capstone paper, we are writing for a busy audience. Therefore, you should read your writing and ask "does this make sense?" Sometimes, I think some of you are blogging so quickly that you do not even apply this level of editing to your writing, and I would encourage you to do so from this point forward.
