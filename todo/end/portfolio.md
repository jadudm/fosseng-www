---
---

Your portfolio is intended to showcase your excellent efforts this term.

I have been recommending that you use the SDS as a possible outline/structure for your portfolio. It is likely to have parts that look like:

* Overview Video
* An Introduction
* The Problem
* Your Design
* Your Implementation
* Challenges Encountered
* Next Steps
* Points of Pride / Success / Personal Highlights

These are just broad-strokes as to what you might include. I am not being overly prescriptive here; by that, I mean *you do not have to use the  sections/categories that I just named*. You need to put together some pages *along those lines* that best highlight the project *you* engaged in. **This is about showcasing your work to potential collaborators/employers/graduate programs in the future**. I am very proud of the effort you have all put into your work this term, and bringing it together in a way that highlights those efforts is important.

You can develop the text and materials in conjunction with your collaborators. You may re-use text (if you agree to) amongst your team. Make sure you contribute excellent effort and support each-other; equitable contribution is key.

So, again: your portfolio is as long or as short as it needs to be. The SDS may (or may not) provide an outline for what you want on your site(s). The spirit of this is to have a URL that you can point to that highlights the hard work and excellent learning you've done this term.

I will happily provide detailed feedback on your portfolio pages; we want them to be of high quality. Send me URLs with feedback requests.

I will also look for a way to sensibly archive them all on the course website; this will be a bit of a trick, but I believe it will be possible.
