---
---

## YouTube Screencast

The video/presentation is intended to be a short, viewable showcase of your work. Roughly:

* 6-8 slides
* Voiceover screencast

If you create the slides, know roughly what you want to say, and sit down with the mic, you should be able to record a 4-8 minute video of those slides without too much effort. Upload to YouTube and you can then embed it as part of the introduction of your portfolio (or anywhere else you want). 

This serves as a nice cornerstone for the portfolio. You can record it in collaboration with your teammates, you can each record your own... whatever makes the most sense for your project. It can then be shared back to your community, which I think will be a valuable thing.

*You might consider licensing your presentation under a CC-BY-SA license*. Ask me if you wonder how to do that.

## Final Exam Slot

During the exam slot, you might then use the same slides to present to the class a brief overview of your project and where you got to. Or, you might take a few minutes and just walk us through your code; again, whatever you think will make the best story for your colleagues. We're not looking to evaluate/judge that presentation so much as hear each-other's stories of success and challenge, and celebrate the work we've done this term.

