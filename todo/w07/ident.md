---
---

Your blog for today should detail the problem you are tackling, and (to the best of your ability) the code that you believe you will be interfacing with.

In this regard, your post should (probably/possibly) include:

  * Links to code in its Github repository
      _Note that you can link to specific lines of code_
  * Snippets of code in your blog post
  * Links to relevant bugs in the tracker
  * Links to/quotes from conversations in mailing lists
  * Links to/quote from relevant blogs

In other words, your research should take you into the details of the problem.

## An Analogy

When you swim the surface of a pool, you learn the width and breadth of it. It becomes a space you have an overview of.

When you dive to the bottom to retrieve a penny, you learn one path through the pool, though you may have never traveled the entire space.

Your problem decomposition is about that deep dive. You're attempting to understand the details of the single problem, and what you're going to need to know to tackle it specifically.
