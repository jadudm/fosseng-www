---
---

For your Software Design Specification, you are going to want to describe the components you are working with in detail. For today, your goal is to have those written out (blogged) in such a way as to be able to describe and discuss them with your teammates.

This is about developing _independent_ (and possibly different) understandings of the software that you then discuss with your partner to develop consensus. Your ability to work together on this problem will be defined, in no small part, by the effort you invest in understanding the software and the problem.

Put another way: you will get out of this process what you put into it.

In class, you will read each-others' component descriptions, discuss/debate to consensus, and be prepared (with detailed notes) to move forward with a common view of the components that you will need to be working with in your implementation.
