---
---

Problem _decomposition_ is where you start deciding what you'll need to do first, second, third, and so on.

The problem you want to tackle is probably not _atomic_ in scope. By this, I mean that your problem or feature implementation probably does not involve changing a single line of code, or a single function. It probably involves understanding how one or more functions, classes, or pieces of code interact with a host of other data structures and functions. As a result, your solution may require changes in multiple places.

This suggests that you are going to need to develop a strategy... you will need to decompose your problem into its smallest parts. In doing so, you are forcing yourself to understand the **system** that you are working in, how the **parts of the system interact**, and **how your changes will impact that system**.

  * [Problem Decomposition Lecture Slides](https://berea.box.com/s/osqsa2r9rvijteszt59eoq7jgi3ry1lo) (U Toronto)
