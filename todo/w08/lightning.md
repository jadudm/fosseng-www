---
---

A lightning talk is a short (3-5 minutes) talk that is given against a set of slides that advance on an automatic timer. There is no room for extension or "running over."

Because we are pressed for time, each pair will produce:

1. A Google Slides presentation that is
1. Set to automatically advance every 30 seconds
    This suggests you will have 6 slides, and each slide effectively says one, _possibly_ two things.
1. Advertised in our #chatter channel
1. In advance of class

In class, we will, rapid-fire, work our way through 15 of these 3-minute presentations. This means that each group will be ready to come up and present when it is their presentation slot, and each member will know which slides are theirs, what they will say on their slides, and be prepared to be 1) brief and 2) clear.

This is challenging. It will probably be humorous. However, I'd appreciate it if you take this opportunity to give me and, really, everyone else in the class an update (if brief) as to where you are in your work and what you are planning on implementing/hacking on for the remainder of the term.
