---
---

Read the [Visual Guide to Version Control](http://betterexplained.com/articles/a-visual-guide-to-version-control/). Class time will begin with a walkthrough of Git and provide an opportunity for Q&A. You should be comfortable by this point with creating directories, creating text files (eg. with *nano*, at the least), and other basic command-line operations. 

If you are not, you need to reach out on the #unstickme channel in Slack and get together with some colleagues and ask some questions.

