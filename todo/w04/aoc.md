---
---

You can get the book online [here](http://artofcommunityonline.org/Art_of_Community_Second_Edition.pdf) (PDF).

There are three chapters that I think will serve us well early on.

* **Chapter 1** is titled "The Art of Community." pp. 1 -- 19.

    Chapter 1 provides a big-picture view of what community means in the context of open software. Skim for an overview.

* **Chapter 3** is titled "Communicating Clearly." pp. 71 -- 96.

    Many of us are extremely ineffective communicators. We demand when we should ask, we state when we should question, we become affronted when we should instead introspect. Read this chapter closely, and start taking it to heart.

* **Chapter 9** is titled "Managing and Tracking Work." pp. 269 -- 309

    Skim this chapter; we'll come back to it. I am looking to you to provide evidence of your work. Note the parts about discomfort working out in the open; your goal this term is to provide evidence of consistent effort, not non-communicative last-minute sprints that fail to support the goals of your pair/group/project.

    While we are skimming this chapter now, we'll cycle back to give it a close read over the weekend. It speaks loudly to the importance of clarity and responsibility in the process of carrying out work in a team environment.

**Chapter 2** is also important, and we'll be reading that over the next weekend; for now, we'll skip it.

## Reflection

On your blog, I would like you to look closely "Communicating Clearly," and reflect on two things:

* What is a moment when you believe you were particularly successful in communicating with either an individual or a group? What about that communication made it excellent?

* What is a moment when you can see that you particularly failed in your communications with an individual or a group? What were your failings in this communication, what did you learn, and what steps have you taken since then to improve?

You might feel hesitant about writing about this kind of thing publicly. Note that these are common types of questions in job interviews -- people ask you where you have succeeded and failed, and how you learned from those moments. If you cannot reflect meaningfully and honestly on your own successes and failures, and you cannot communicate about them clearly, then you are probably not the kind of team member that is being sought in mixed, diverse, collaborative teams in the software industry today.

*There are limits, of course, to how personal you should be in your blog. (I believe that to be the case, anyway.) You should not use names, you should not betray the confidences of others, nor should you stray into the realm of the deeply personal. If you are uncertain as to whether you are blogging about something that might be "too much" for the public sphere, you are always welcome to ask.*
