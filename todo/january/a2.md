---
---

# TL;DR

Your goals for the weekend are multifold.

1. Learn to create directories, remove directories, move them around
1. Learn to create, remove, edit, move files
1. Learn the basics of Markdown
1. Mount server from local machine.
1. Join our Slack team.
1. Create a weblog.
1. First post.

# Reading: CLI the Hard Way

[http://cli.learncodethehardway.org/book/](http://cli.learncodethehardway.org/book/)

Like it says.

Explore to some depth; no requirement as to how far you explore. You'll probably need to know most of this at some point... certainly, creating directories, listing files, etc. will be bread-and-butter stuff.

## Goal: Directory Management

On the server, create a directory hierarchy for your work in CSC. For example, you should decide what folders you want for each of your classes, what they'll be named, and where they will be. Do you want a "courses" folder that contains all of your work, and one folder per course underneath that in the filesystem tree? Should there be separate directories for coding and writing projects?

You can always change it later, but for now, use this as a reason to create a set of directories that you can work with.

## Time Tracking

Create a file for CSC 486 called "worklog.md". The 'md' extension is often used for Markdown files:

[https://daringfireball.net/projects/markdown/](https://github.com/jadudm/hello-world)

Your worklog is an enumerated list of times/durations of work you carry out for this course, and short descriptions of the work that you did during that time. It will become a journal of your efforts, where each journal entry is roughly one or two sentences.

This serves two purposes: first, it tracks time (which we want), and second, it gets you working in Markdown, which is commonly used for a number of documentation tasks nowadays. (This entire website is written in Markdown, FWIW.)

## Mount carter.cs from Windows

Finally, if you want to be able to use a native Windows text editor for your code authoring, but work directly on Carter (so that you don't have to move files back and forth between your laptop and the server), you should install and set up **win-sshfs**. 

[http://igikorn.com/sshfs-windows-8/](http://igikorn.com/sshfs-windows-8/)

Those directions will get you started. You can grab a community-member's updated version of the original software here:

[https://github.com/dimov-cz/win-sshfs](https://github.com/dimov-cz/win-sshfs)

and they make the binary releases available here:

[https://github.com/dimov-cz/win-sshfs/releases](https://github.com/dimov-cz/win-sshfs/releases)

You'll need the dokany library as well:

[https://github.com/dokan-dev/dokany](https://github.com/dokan-dev/dokany)

and, again, the binary releases of this library:

[https://github.com/dokan-dev/dokany/releases](https://github.com/dokan-dev/dokany/releases)

## Slack: For Coordination

We'll give it a try. I'm pretty sure it violates the FOSS ethos, because it isn't a free application. If we were good citizens, we'd use a mail server that was free-and-open, hosted on our own server, and we wouldn't tie ourselves to a proprietary application.

I, however, do not have time to do that in addition to finishing configuring and managing our servers. So, we will use Slack for internal communication and collaboration. Note: this means that you lose your public trail of participation and collaboration, so this really should only be used for things pertaining to the mechanics of the course, or internal organization and meetings. (In other words, I think the TA and I will use this to keep up with you, but you shouldn't use it to do all of your work as a team.)

I may reverse my thoughts on Slack, and kill this team, at some point in the near future. I'm moving fast to make sure we have the tools we need...

[https://fosseng.slack.com/](https://fosseng.slack.com/)

## Create a Weblog

Create a weblog. Wordpress is a good choice (at wordpress.org), or you can use Blogger, Medium, Ghost... 

Just make sure that your choice has support for publishing Atom or RSS feeds. When you create your blog, post a URL to your blog on the #blog channel in Slack.

You are not required to make your public presence one that is based on your real name. Personally, I am "jadudm" in almost all forums, because that was my undergraduate username. You may choose otherwise. (In fact, you should probably not choose "jadudm," as that would get confusing.)

## First Post

Research the projects that have been suggested for your open source deep dive; which look particularly appealing? Which are you less inclined to be interested in? Why?

We'll talk more about blog posts; for now, remember that you have a public audience. I'd expect that you're writing around 250 words for a typical post, but really... write as much as you feel is necessary.

I have more to say on the topic of blogging, but for now, just try not to post anything that will keep you from getting a job someday...

