---
---

Randall Monroe, the author of XKCD, a fun, geeky web comic, has this to say on the subject of Real Programmers:

<div class = 'text-center'>
  <img src = "http://imgs.xkcd.com/comics/real_programmers.png ">
</div>

Don't forget to hover your cursor over his comic for the alternate text...

Also, you might note [http://eniacprogrammers.org/](http://eniacprogrammers.org/), which collects up information about the Real Programmers of ENIAC, the first general purpose computer (1946).

I can't find the documentary (*The Computers*), but I was able to find this six minute video on YouTube. Bartik is featured in the video... one of the tables in our lab carries her name.

<div class = "text-center">
  <iframe width="640" height="480" src="https://www.youtube.com/embed/aPweFhhXFvY?rel=0" frameborder="0" allowfullscreen></iframe>
</div>