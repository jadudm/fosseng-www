---
title: Software
resource: local
layout: default
---

There are a lot of different tools we'll be using this term. People have complained about this in the past. I'm afraid there's nothing I can really do about that... we need tools to do what we do!

## Bitbucket or GitHub. Or, Both.

I have dreams of using [Bitbucket](https://bitbucket.org/) for managing code and collaboration. You should create an account there, and we'll see how things go.

Or GitHub.

## Cel.ly


<div class="text-center">
  <script src='https://cel.ly/embed/CompOrg.js'></script>
</div>

Celly will be used for alerts and updates. Students have repeatedly asked for ways they can be notified about changes to the website and other course announcements via *anything but* email. Celly provides me with a way to post messages that **you can choose** to have received via SMS, email, or via an app you can install (Android or IOS).

You can create an account using Twitter/Facebook/Google credentials, but **your profile must use your berea.edu address**. This is how I am making sure only Berea students can join into our "cells." You will want to join the cell <b>@CompOrg</a> after you create your account.

<div class="text-center">
  <iframe width="640" height="360"
          src="//www.youtube.com/embed/CHAU4qjD-i4?rel=0"
          frameborder="0"
          allowfullscreen
          ></iframe>
<br/>
</div>
