---
title: Assignments
resource: local
layout: default
---

{% for todo in site.data.todo %}

{% unless todo[1].hide %}

<div class="row">
  <div class="col-md-11 offset-1">
    <h3>{{todo[1].question}}</h3>
  </div>
</div>
  {% for part in todo[1].parts %}
  {% assign theID = part.title | remove: " " | remove: "." | remove: "," | remove: "-" | remove: "+" | remove: "&" %}
  {% capture datelength %}{{ part.date | size | minus: 9 }}{% endcapture %}
  {% capture date %}{{part.date}}{% endcapture %}
  {% if datelength contains '-' %}
    {% if site.data.course.classtime %}
      {% capture date %}{{part.date}}{{site.data.course.classtime}}{% endcapture %}
    {% else %}
      {% capture date %}{{part.date}}2359{% endcapture %}
    {% endif %}
  {% endif %}
  {% unless part.inclass %}
<div id="fade-{{theID}}-{{date}}">
  <div class="row"> 
    <div class="col-md-3" id="{{theID}}-{{date}}"> </div>
    <div class="col-md-8" style="padding-bottom: 10px;">
    <a href="{{site.base}}/todo/{{todo[0]}}/{{part.file}}.html">
      {{part.title}}
    </a>
    <span style="font-size: 80%; padding: 10px;" class="pull-right" id="{{theID}}-prettydate"> </span>
  </div>
  </div>
</div>
<script type='text/javascript'>
  function {{theID}}{{date}} () {
    dueIn("{{theID}}-{{date}}", "{{date}}", "{{part.inclass}}", "", "90%");
    prettyDate("{{theID}}-prettydate", "{{date}}", "");
  }
  {{theID}}{{date}}();
</script>
  {% endunless %}
{% endfor %}

{% endunless %}
{% endfor %}
