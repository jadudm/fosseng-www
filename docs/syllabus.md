---
title: Software Engineering Syllabus
resource: local
layout: default
numlearningobj: six
---


{% include twocol label="Course" value=site.data.course.name %}
{% include twocol label="Number" value=site.data.course.number %}
{% include twocol label="Instructor" value="Matt Jadud" %}
{% include twocol label="Office" value="Danforth Tech 102B" %}
{% include twocol label="Phone" value=site.data.course.phone %}
{% include twocol label="Email" value="jadudm" %}


{% callout info %}
We will explore the details of this syllabus together throughout the term.

Elements of this syllabus are inspired by the work of OSD 600 at Seneca College, <a href="http://zenit.senecac.on.ca/wiki/index.php/Open_Source@Seneca:Copyrights">licensed under a Canadian CC-BY and the Open Publication License</a>.
{% endcallout %}

## Course Description

Software Engineering will introduce students to the practical aspects of developing software in teams. Specifically, we will explore social and technological aspects of this process by contributing directly to an existing, large open source project. We will learn the tools, the techniques, and the strategies that are common to all developers working in teams through this semester-long exercise. Students will primarily be assessed on their deliverables to the project as well as weekly blogs and an open source project case study. The end-product is intended to serve as a visible portfolio of work for students interested in pursuing further work in computing.

## Course Goals

These goals provide the "big picture" of our work developing and contributing open source software with collaborators from around the world.

1. **Open Community and Culture**. <br>

    Open software happens in a rich cultural context. It is important not only that we understand who we are working with, but the cultural values they embrace and exemplify through their work within a community.

1. **Tools and Technologies**. <br>

    All open projects involve the use of tools. Lots, and lots, of tools. Whether they are wikis, or chat systems, or mailing lists, or version control systems, or bug trackers, or... the list goes on and on. This course will introduce you to some of these tools, and help you establish the mindset that you will be learning new tools for the rest of your life as you continue to work in computing.

1. **Development and Process**. <br>

    Developing software is a collaborative process. It involves communication with others, agreement on design, agreement on style, and agreement on when to agree and when to disagree. These dialogs all have a process, and the technical act of designing, implementing, committing, testing, and publishing software all have processes as well. Understanding these layers of human- and technical-oriented process are critical to modern software development. 

1. **Collaboration and Communication**. <br>

    Working with people is hard. It involves patience, kindness, humility, and an awareness of self that many people working in technology do not have or even realize they should focus on developing. We will read about, practice, and engage in readings and discussions about who we are, how we work with others, and how to reflect on those interactions productively in the context of collaborative, distributed software development.


<!-- http://teachingcommons.depaul.edu/Course_Design/developing_a_course/goals.html -->
<!-- http://ets.tlt.psu.edu/learningdesign/objectives/writingobjectives -->
<!-- http://www.cmu.edu/teaching/designteach/design/learningobjectives-samples/index.html -->
<!-- http://www.cmu.edu/teaching/designteach/design/learningobjectives.html -->

{% comment %}
Learning objectives involve the kinds of knowledge you will gain regarding electricity and electronics, the ability to apply and integrate it, as well as the human connections and opportunities for lifelong learning in the field. As the course progresses, we will work together to develop answers to the following questions.
{% endcomment %}

{% comment %}
## Course Learning Objectives

Learning objectives are the assessable points of learning in the course. They are organized under our large, sweeping course goals in a rough order of increasing challenge. We will generally begin the semester with learning goals that are lower on the list, and work our way up through them over the course of the term.

**LO** is short for *learning outcome*.


### Open Community and Culture

**LO 1.1** TBA

### Tools and Technologies

**LO 2.1** TBA

### Development and Process

**LO 3.1** TBA

### Collaboration and Communication

**LO 4.1** TBA
{% endcomment %}

## Grade Distribution

{% comment %}
75% - Project Deliverables (e.g., code, documents), marked in terms of quality, quantity, etc. Your project will be marked at four milestone releases, the number and values being:
15% - 0.1 Release (Due Sept 26)
20% - 0.2 Release (Due Oct 17)
20% - 0.3 Release (Due Nov 14)
20% - 0.4 Release (Due Dec 5)
15% - Blog. You will be marked on your blog's quality, depth of explanation, frequency of update, etc. You are expected to blog weekly throughout the course.
5% - FSOSS 2014 Report. You will be marked on a report that will be based on research and analysis you will do at FSOSS 2014.
5% - 2014 Open Source Project Case Study. You will be marked on a class presentation about an open source project you study, its community, code, and culture.
{% endcomment %}

<div class = "">
  <div style = "width: 50%; margin: 0 auto;">
  <div class = "row">
    <div class = "col-md-6">
      <b>Category</b>
    </div>
    <div class = "col-md-4">
      <b>Weight</b>
    </div>
  </div>
  <div class = "col-md-10">
    <hr>
  </div>
  <div class = "row">
    <div class = "col-md-6">
      Developmental Deliverables (code, reports, responses, etc.)
    </div>
    <div class = "col-md-4">
      50%
    </div>
  </div>
  <div class = "row">
    <div class = "col-md-6">
      Final Portfolio
    </div>
    <div class = "col-md-4">
      25%
    </div>
  </div>
  <div class = "row">
    <div class = "col-md-6">
      Blog
    </div>
    <div class = "col-md-4">
      10%
    </div>
  </div>

  <div class = "row">
    <div class = "col-md-6">
      Case Study
    </div>
    <div class = "col-md-4">
      5%
    </div>
  </div>

 <div class = "row">
   <div class = "col-md-6">
     Peer Evaluation
   </div>
   <div class = "col-md-4">
     10%
   </div>
 </div>
  
</div>
</div>

## Expectations and Attendance

To achieve the learning outcomes in this course, you may need to alter some of your prior behaviors.

<style type = "text/css">
tr.line td{
border-bottom: 1px solid #B4B5B0;
}
</style>

<table width = '80%' align="center" >
  <tr style="background: #eee;" class = "line">
    <td style="border-bottom-style: solid;"> Traditional Classroom <br/> <small>Students shift from...</small></td>
    <td width="5%" style="padding: 1em;"> </td>
    <td> Collaborative Classroom <br/> <small>to...</small></td>
  </tr>
  <tr class = "line">
    <td>Listener, observer, and note taker</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Active problem solver, contributor, and discussant</td>
  </tr>
  <tr class = "line">
    <td>Low or moderate expectations of preparation for class</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>High expectations of preparation for class</td>
  </tr>

  <tr class = "line">
    <td>Private presence in the classroom with few or no risks</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Public presence with many risks</td>
  </tr>

  <tr class = "line">
    <td>Attendance dictated by personal choice</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Attendance dictated by community expectation</td>
  </tr>

  <tr class = "line">
    <td>Competition with peers</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Collaborative work with peers</td>
  </tr>

  <tr class = "line">
    <td>Responsibilities and self-definition associated with learning independently</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Responsibilities and self-definition associated with learning interdependently</td>
  </tr>

  <tr class = "line">
    <td>Seeing teachers and texts as the sole sources of authority and knowledge</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Seeing peers, self, and the community as additional and important sources of authority and knowledge</td>
  </tr>

</table>

## Revision

Work must be turned in on time to be eligible for revision.

Work turned in late can never achieve an Excellent rating.

## Attendance

We will be actively engaging during class time in pair programming and group discussion activities throughout the term. Attendance is part of being professional as a student; each missed class may cost you up to 3% off your final grade.

If you arrive particularly late, leave particularly early, or fail to participate in class, I will consider you absent.

If you miss class for reasons that are not considered legitimate, you are responsible for getting caught up. That does not mean you should write me, apologize for being absent, and then ask me what you missed. Start with a kind classmate, and see how you fare.

More than 6 unexcused absences will be grounds for failure of the course, as this represents two full weeks of learning. Too much time and effort is expended by your faculty, TAs, and your classmates (working along side you in your learning) to accept absences from the learning activities of this course. At six unexcused absences, you may be asked to leave.

### Legitimate Reasons You Might Be Absent

If you legitimately cannot be present, you will find a way to communicate that to me in advance.

 * If you have flu-like symptoms, email me and visit the health center. The Center for Disease Control recommends you stay home for at least 24 hours after the symptoms pass—RESTING. Without evidence from the health center of your visit, you are not excused.

 * Job Interviews

 * Documented Family Emergencies / Deaths
 
 * Others as discussed/agreed upon with the instructor of record.

## Class Atmosphere

I want many things for students in my classes, and I very much want you to help me achieve these goals.

* I want our laboratory to be a relaxed environment where you are **comfortable trying new things** and (sometimes) failing. By "failure" I do not mean "receiving an F," but I do mean that you try things, make mistakes, and learn from them. The last bit---learning from our mistakes---is the critical part. Neither I, nor you, nor your classmates should put down or belittle a classmate for trying.

* I want you to **look forward to {{site.short}} because it is fun**. We should be comfortable with each other---humor and laughter makes the day go faster and better. That said...

* We should **work hard, and be proud of that effort**. For me, a "fun" day is one where I've worked hard and improved myself. I have done my best to design a course that will be fun because it challenges us to work hard and do new and interesting things.

* **Respect matters**. Respect for each-other, regardless of where we are from and where we are on our life journey is of utmost importance. I have a great deal of respect for your effort as a student; I show that respect by challenging you to extend your limits, and supporting you to the best of my ability as you take risks and engage with the course throughout the semester.

## Statement Regarding Title IX

Under Title IX of the Education Amendments of 1972, pregnant and parenting students may be afforded certain accommodations regarding their educational experience.  If you believe that pregnancy or pregnancy-related conditions are likely to impact your participation in this course, please contact Berea’s Title IX Coordinator, Katie Basham, to discuss appropriate accommodations.  She may be reached at Katherine_basham@berea.edu or 859.985.3606.
 

## Statement Regarding Disability

Berea College values diversity and inclusion and seeks to create a climate of mutual respect and full participation. My goal is to create learning environments that are accessible, equitable, and inclusive. If you encounter barriers based on the impact of a disability or health condition, please let me and Disability & Accessibility Services (DAS, 111 Lincoln Hall, 859-985-3237, lisa.ladanyi@berea.edu)) know immediately so that we can determine if there is a design adjustment that can be made to the course or if accommodations might be needed to overcome the barriers. Together we can explore all of your options and establish how to best coordinate accommodations for this course.

Students are welcome to discuss sensitive topics – including disability-related concerns – with me, which will be kept confidential. However, you must go through DAS procedures to obtain accommodations.

{% callout danger %}
This syllabus is a living document. It may change.
{% endcallout %}
